﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera_height : MonoBehaviour
{
    public float height; // переменная в которую из функции Update поставляется значение высоты камеры


    void Update() // апдейтится каждый фрейм
    {
        height = GameObject.FindGameObjectWithTag("MainCamera").transform.position.y; // Ищем объект с тегом MainCamera и затем в переменную height поставляются значения высоты камеры - координаты y
        AkSoundEngine.SetRTPCValue("RTPC_ext_camera_height", height);
    }
}
