﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class footsteps : MonoBehaviour
{
    public AK.Wwise.Event MyEvent;
    private string _surfaceType;
    private float _trackTime;
    public string SwitchGroup_surface = "surface_type";
    public string SwitchGroup_locomotion = "locomotioin";



    // Start is called before the first frame update 
    private void Update()
    {
        if (Time.time - _trackTime < 0.1f) return;

        //print("Raycast"); 
        RaycastHit hit;
        if (Physics.Raycast(transform.position + transform.up, -transform.up, out hit, 2.0f))
        {
            _surfaceType = hit.collider.tag;
            //print(hit.collider.tag); 
        }

        _trackTime = Time.time;
    }


    public void footstep_walking()
    {
        AkSoundEngine.SetSwitch(SwitchGroup_locomotion, "walk", gameObject);
        if (_surfaceType == "Dirt")
        {
            AkSoundEngine.SetSwitch(SwitchGroup_surface, "sand", gameObject);
            MyEvent.Post(gameObject);
        }
        if (_surfaceType == "Grass")
        {
            AkSoundEngine.SetSwitch(SwitchGroup_surface, "grass", gameObject);
            MyEvent.Post(gameObject);
        }
        if (_surfaceType == "Stone")
        {
            AkSoundEngine.SetSwitch(SwitchGroup_surface, "stone", gameObject);
            MyEvent.Post(gameObject);
        }
    }

    public void footstep_running()
    {
        AkSoundEngine.SetSwitch(SwitchGroup_locomotion, "run", gameObject);
        if (_surfaceType == "Dirt")
        {
            AkSoundEngine.SetSwitch(SwitchGroup_surface, "sand", gameObject);
            MyEvent.Post(gameObject);
        }
        if (_surfaceType == "Grass")
        {
            AkSoundEngine.SetSwitch(SwitchGroup_surface, "grass", gameObject);
            MyEvent.Post(gameObject);
        }
        if (_surfaceType == "Stone")
        {
            AkSoundEngine.SetSwitch(SwitchGroup_surface, "stone", gameObject);
            MyEvent.Post(gameObject);
        }
    }


}
